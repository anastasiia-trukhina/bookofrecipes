﻿using BookOfRecipes.BLL.Interfaces;
using BookOfRecipes.Common.Constants;
using BookOfRecipes.Data.Account;
using BookOfRecipes.WebApi.Models.Account;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BookOfRecipes.WebApi.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly IAccountService _service;
        private readonly ILogger _logger;

        public AccountController(IAccountService service, ILogger<AccountController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPut]
        [Route("GetToken")]
        public async Task<GetTokenResponse> GetTokenAsync([FromBody] GetTokenRequest request)
        {
            try
            {
                var response = await _service.GetTokenAsync(request.Email, request.Password);

                var result = new GetTokenResponse
                {
                    AccessToken = response.AccessToken,
                    TokenType = response.TokenType,
                    ExpiresIn = response.ExpiresIn,
                    IsSuccess = !string.IsNullOrEmpty(response.AccessToken)
                };

                if (result.IsSuccess)
                {
                    HttpContext.Current.Response.Cookies.Add(new HttpCookie(AuthenticationConstants.TokenCookieName, result.AccessToken));
                }

                return result;
            }
            catch (Exception e)
            {
                return new GetTokenResponse
                {
                    IsSuccess = false,
                    ErrorDescription = e.Message
                };
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("RegisterNewClient")]
        public RegisterNewClientResponse RegisterNewClient([FromBody] RegisterNewClientRequest request)
        {
            try
            {
                _service.RegisterNewClient(request);

                var result = new RegisterNewClientResponse
                {
                    IsSuccess = true
                };

                return result;
            }
            catch (Exception e)
            {
                return new RegisterNewClientResponse
                {
                    IsSuccess = false,
                    ErrorDescription = e.InnerException.ToString()
                };
            }
        }
    }
}