﻿namespace BookOfRecipes.WebApi.Models.Account
{
    public class GetTokenRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}