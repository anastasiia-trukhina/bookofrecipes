﻿using System;

namespace BookOfRecipes.WebApi.Models
{
    public class Recipe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IngredientId { get; set; }
        public int StepId { get; set; }
        public int AuthorId { get; set; }
        public int? CommentId { get; set; }
        public int? RatingId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }
        public bool IsPublic { get; set; }
        public bool IsAudience { get; set; }
        public int? AudienceId { get; set; }
        public int TypeOfDish { get; set; }
        public int? TagId { get; set; }
        public int? ResolveId { get; set; }
    }
}