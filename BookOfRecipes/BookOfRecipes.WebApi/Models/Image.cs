﻿namespace BookOfRecipes.WebApi.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] File { get; set; }
    }
}