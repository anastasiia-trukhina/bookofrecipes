﻿using System;

namespace BookOfRecipes.WebApi.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ParticipantId { get; set; }
        public int? BannerId { get; set; }
        public int? LogoId { get; set; }

        public virtual Image Banner { get; set; }
        public virtual Image Logo { get; set; }
    }
}