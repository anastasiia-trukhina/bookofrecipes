﻿namespace BookOfRecipes.WebApi.Models
{
    public class Rating
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public int AuthorId { get; set; }
    }
}