﻿namespace BookOfRecipes.WebApi.Models
{
    public class Subscription
    {
        public int Id { get; set; }
        public int? SubscriptionId { get; set; }
        public bool? IsNotified { get; set; }
    }
}