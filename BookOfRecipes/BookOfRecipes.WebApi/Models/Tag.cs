﻿namespace BookOfRecipes.WebApi.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AuthorId { get; set; }
        public bool IsVerified { get; set; }
        public int? ResolveId { get; set; }
    }
}