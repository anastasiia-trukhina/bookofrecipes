﻿namespace BookOfRecipes.WebApi.Models
{
    public class BaseResponse
    {
        public bool IsSuccess { get; set; }
        public string ErrorDescription { get; set; }
    }
}