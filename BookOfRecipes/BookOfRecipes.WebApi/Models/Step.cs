﻿namespace BookOfRecipes.WebApi.Models
{
    public class Step
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public int? ImageId { get; set; }
    }
}