﻿namespace BookOfRecipes.WebApi.Models
{
    public class Archive
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ResipeId { get; set; }
    }
}