﻿namespace BookOfRecipes.WebApi.Models
{
    public class Audience
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
    }
}