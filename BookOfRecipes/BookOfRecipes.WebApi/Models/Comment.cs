﻿namespace BookOfRecipes.WebApi.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int AuthorId { get; set; }
    }
}