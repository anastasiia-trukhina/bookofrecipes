﻿namespace BookOfRecipes.WebApi.Models
{
    public class Album
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public bool IsPublic { get; set; }
        public bool IsAudience { get; set; }
        public int? AudienceId { get; set; }
    }
}