﻿namespace BookOfRecipes.WebApi.Models
{
    public class Ingredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public bool IsVerified { get; set; }
        public int? ResolveId { get; set; }
    }
}