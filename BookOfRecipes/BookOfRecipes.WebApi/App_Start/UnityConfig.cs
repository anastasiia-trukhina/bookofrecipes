using AutoMapper;
using BookOfRecipes.BLL.Interfaces;
using BookOfRecipes.BLL.Services;
using BookOfRecipes.DAL.Interfaces;
using BookOfRecipes.DAL.Storages;
using BookOfRecipes.WebApi.Controllers;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.WebApi;

namespace BookOfRecipes.WebApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = BuildUnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            var mapper = GetConfiguredMapper();

            container.RegisterType<IAccountService, AccountService>()
                    .RegisterType<IAccountStorage, AccountStorage>()
                    .RegisterInstance(mapper, new ContainerControlledLifetimeManager())
                    .RegisterInstance<ILoggerFactory>(new LoggerFactory(), new ContainerControlledLifetimeManager())
                    .RegisterType(typeof(ILogger<>), typeof(Logger<>))
                    .Resolve<ILogger<AccountController>>();

            return container;
        }

        private static IMapper GetConfiguredMapper()
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new Common.Mapping.BookOfRecipesProfile());
            });

            var mapper = mapperConfig.CreateMapper();
            return mapper;
        }
    }
}