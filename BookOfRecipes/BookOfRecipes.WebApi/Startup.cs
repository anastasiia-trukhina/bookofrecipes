﻿using BookOfRecipes.BLL.Interfaces;
using BookOfRecipes.Data.Account;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

[assembly: OwinStartup(typeof(BookOfRecipes.WebApi.Startup))]

namespace BookOfRecipes.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new MyOAuthAuthorizationServerProvider(GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IAccountService)) as IAccountService),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                AllowInsecureHttp = false
            });

            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        public class MyOAuthAuthorizationServerProvider : OAuthAuthorizationServerProvider
        {
            private readonly IAccountService _service;

            public MyOAuthAuthorizationServerProvider(IAccountService service)
            {
                _service = service;
            }

            public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
            {
                var clientId = context.Parameters["client_id"];
                var clientSecret = context.Parameters["client_secret"];

                if (clientId != null && clientSecret != null)
                {
                    var user = _service.GetUserByEmailAndPassword(clientId, clientSecret);
                    if (user != null)
                    {
                        context.OwinContext.Set<User>("oauth:client", user);
                        context.Validated(clientId);
                    }
                }
                return Task.FromResult<object>(null);
            }

            public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
            {
                var user = context.OwinContext.Get<User>("oauth:client");
                if (user != null)
                {
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    if (user?.Id == 1)
                    {
                        identity.AddClaim(new Claim("role", "admin"));
                    }
                    else
                    {
                        identity.AddClaim(new Claim("role", "user"));
                    }
                    context.Validated(identity);
                }
                return Task.FromResult<object>(null);
            }
        }
    }
}
