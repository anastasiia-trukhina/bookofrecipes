﻿using BookOfRecipes.DAL.Interfaces;
using System.Linq;

namespace BookOfRecipes.DAL.Storages
{
    public class AccountStorage : IAccountStorage
    {
        public AccountStorage()
        {
        } 

        public user GetUserByEmailAndPassword(string email, string password)
        {
            user result = null;
            using (var dbContext = new BookOfRecipesContext())
            {
                dbContext.Database.Connection.Open();
                result = dbContext.users.FirstOrDefault(x => x.Email == email && x.Password == password);
                dbContext.Database.Connection.Close();
            }
            return result;
        }

        public void RegisterNewClient(user user)
        {
            using (var dbContext = new BookOfRecipesContext())
            {
                dbContext.Database.Connection.Open();
                dbContext.users.Add(user);
                dbContext.SaveChanges();
                dbContext.Database.Connection.Close();
            }
        }
    }
}
