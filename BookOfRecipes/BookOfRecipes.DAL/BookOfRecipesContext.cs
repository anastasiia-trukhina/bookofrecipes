﻿namespace BookOfRecipes.DAL
{
    public class BookOfRecipesContext : bookOfRecipesEntities
    {
        public BookOfRecipesContext()
            : base()
        {
            this.Configuration.ProxyCreationEnabled = false;
        }
    }
}
