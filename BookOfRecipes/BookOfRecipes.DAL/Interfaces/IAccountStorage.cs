﻿namespace BookOfRecipes.DAL.Interfaces
{
    public interface IAccountStorage
    {
        user GetUserByEmailAndPassword(string email, string password);

        void RegisterNewClient(user user);
    }
}
