﻿using BookOfRecipes.Data.Account;
using System.Threading.Tasks;

namespace BookOfRecipes.BLL.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Get token by auth pair
        /// </summary>
        /// <param name="email">User's email</param>
        /// <param name="password">User's password</param>
        /// <returns>Access token as string</returns>
        Task<Token> GetTokenAsync(string email, string password);

        /// <summary>
        /// Get user by auth pair
        /// </summary>
        /// <param name="email">User's email</param>
        /// <param name="password">User's password</param>
        /// <returns>User</returns>
        User GetUserByEmailAndPassword(string email, string password);

        /// <summary>
        /// Register new client
        /// </summary>
        /// <param name="user">User's data</param>
        void RegisterNewClient(User user);
    }
}
