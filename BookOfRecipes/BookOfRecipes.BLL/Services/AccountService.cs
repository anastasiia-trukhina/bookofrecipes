﻿using AutoMapper;
using BookOfRecipes.BLL.Interfaces;
using BookOfRecipes.DAL.Interfaces;
using BookOfRecipes.DAL;
using BookOfRecipes.Data.Account;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace BookOfRecipes.BLL.Services
{
    public class AccountService : IAccountService
    {
        private RestClient _client;

        private readonly IAccountStorage _accountStorage;
        private readonly IMapper _mapper;

        public AccountService(IAccountStorage accountStorage, IMapper mapper)
        {
            _accountStorage = accountStorage;
            _mapper = mapper;
        }

        public async Task<Token> GetTokenAsync(string email, string password)
        {
            try
            {
                _client = new RestClient("https://localhost:44313/token");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", $"grant_type=password&client_id={email}&client_secret={password}", ParameterType.RequestBody);
                var response = await _client.ExecuteAsync(request);

                return JsonConvert.DeserializeObject<Token>(response.Content);
            }
            catch (Exception e)
            {
                throw new Exception($"An error while get token for user {email}", e);
            }
        }

        public User GetUserByEmailAndPassword(string email, string password)
        {
            var userDAL = _accountStorage.GetUserByEmailAndPassword(email, password);
            var userBLL = _mapper.Map<User>(userDAL);

            return userBLL;
        }

        public void RegisterNewClient(User user)
        {
            var userDAL = _mapper.Map<user>(user);
            _accountStorage.RegisterNewClient(userDAL);
        }
    }
}
