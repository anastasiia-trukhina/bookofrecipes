﻿using AutoMapper;
using BookOfRecipes.DAL;
using BookOfRecipes.Data.Account;

namespace BookOfRecipes.Common.Mapping
{
    public class BookOfRecipesProfile : Profile
    {
        public BookOfRecipesProfile()
        {
            CreateMap<user, User>();
            CreateMap<User, user>();
        }
    }
}
