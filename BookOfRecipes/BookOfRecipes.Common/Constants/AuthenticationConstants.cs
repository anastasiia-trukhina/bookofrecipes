﻿namespace BookOfRecipes.Common.Constants
{
    public static class AuthenticationConstants
    {
        public static string TokenCookieName = "BookOfRecipes.AccessToken";
    }
}
