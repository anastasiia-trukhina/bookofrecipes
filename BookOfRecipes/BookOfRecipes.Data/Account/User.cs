﻿using System;

namespace BookOfRecipes.Data.Account
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronimyc { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public int? GroupId { get; set; }
        public int? AlbumIds { get; set; }
        public int? FriendId { get; set; }
        public int? ArchiveId { get; set; }
        public int? SubscriptionId { get; set; }
    }
}
