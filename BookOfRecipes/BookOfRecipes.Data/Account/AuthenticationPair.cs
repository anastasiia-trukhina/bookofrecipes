﻿namespace BookOfRecipes.Data.Account
{
    public class AuthenticationPair
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
